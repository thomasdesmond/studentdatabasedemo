﻿using System;
using SQLite;

namespace DatabaseDemo
{
    public class StudentModel
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public string Grade { get; set; }
    }

}
