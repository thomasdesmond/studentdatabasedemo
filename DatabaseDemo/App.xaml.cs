﻿using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace DatabaseDemo
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new MainPage();
        }

        static StudentDatabase studentDatabase;
        public static StudentDatabase StudentDatabaseInstance
        {
            get
            {
                if (studentDatabase == null)
                {
                    studentDatabase = new StudentDatabase(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "StudentSQLite.db3"));
                }
                return studentDatabase;
            }
        }


        static TeacherDatabase teacherDatabase;
        public static TeacherDatabase TeacherDatabaseInstance
        {
            get
            {
                if (teacherDatabase == null)
                {
                    teacherDatabase = new TeacherDatabase(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "TeacherSQLite.db3"));
                }
                return teacherDatabase;
            }
        }


        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
