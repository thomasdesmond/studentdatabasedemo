﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;

namespace DatabaseDemo
{
    public class StudentDatabase
    {
        readonly SQLiteAsyncConnection studentDatabase;


        public StudentDatabase(string dbPath)
        {
            studentDatabase = new SQLiteAsyncConnection(dbPath);
            studentDatabase.CreateTableAsync<StudentModel>().Wait();
        }


        public Task<int> SaveStudentAsync(StudentModel item)
        {
            if (item.ID != 0)
            {
                return studentDatabase.UpdateAsync(item);
            }
            else
            {
                return studentDatabase.InsertAsync(item);
            }
        }

        public Task<List<StudentModel>> GetAllStudents()
        {
            return studentDatabase.QueryAsync<StudentModel>("SELECT * FROM [StudentModel]");
        }
    }

   
}
