﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;

namespace DatabaseDemo
{
    public class TeacherDatabase
    {
        readonly SQLiteAsyncConnection teacherDatabase;


        public TeacherDatabase(string dbPath)
        {
            teacherDatabase = new SQLiteAsyncConnection(dbPath);
            teacherDatabase.CreateTableAsync<TeacherModel>().Wait();
        }

        public Task<int> SaveTeacherAsync(TeacherModel item)
        {
            if (item.ID != 0)
            {
                return teacherDatabase.UpdateAsync(item);
            }
            else
            {
                return teacherDatabase.InsertAsync(item);
            }
        }

        public Task<List<TeacherModel>> GetAllTeachers()
        {
            return teacherDatabase.QueryAsync<TeacherModel>("SELECT * FROM [TeacherModel]");
        }
    }
}
