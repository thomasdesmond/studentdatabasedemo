﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DatabaseDemo
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void Handle_SendStudentDataToDatabase(object sender, System.EventArgs e)
        {
            var student = new StudentModel
            {
                Name = nameEntry.Text,
                Class = classEntry.Text,
                Grade = gradeEntry.Text,
            };
            await App.StudentDatabaseInstance.SaveStudentAsync(student);
        }


        async void Handle_SendTeacherDataToDatabase(object sender, System.EventArgs e)
        {
            var teacher = new TeacherModel
            {
                Name = teacherNameEntry.Text,
                Class = teacherClassEntry.Text,
                Semester = tearcherSemesterEntry.Text,
            };
            await App.TeacherDatabaseInstance.SaveTeacherAsync(teacher);
        }

        async void Handle_GetAllDatabaseEntries(object sender, System.EventArgs e)
        {
            var allStudents = await App.StudentDatabaseInstance.GetAllStudents();
        }

        async void Handle_GetAllTeacherEntries(object sender, System.EventArgs e)
        {
            var allTeachers = await App.TeacherDatabaseInstance.GetAllTeachers();
        }
    }
}
