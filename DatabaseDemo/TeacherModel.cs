﻿using System;
using SQLite;

namespace DatabaseDemo
{
    public class TeacherModel
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public string Semester { get; set; }
    }
}
